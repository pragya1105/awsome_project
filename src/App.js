import './App.css';
import TableComp from './table';
import { PersonRounded, CloudUploadOutlined } from '@material-ui/icons';
function App() {
  return (
	<div className="App" style={{ 'textAlign': 'left' }}>
	<div className='topheader'>
		<div className='logo-sec'>
			Logo
		</div>
		<div className='acount-sec'>
			<span
				className='upload'
				onClick={()=>{
					console.log('upload');
				}}
			>
				<CloudUploadOutlined/>
				Upload Lease
			</span>
			<button
				onClick={(e)=>{
					console.log('User click');
				}}
			><PersonRounded/> User</button>
		</div>
	</div>
      <div className='header' >
			Dashboard
	  </div>
	  <div>
		<TableComp />
	  </div>
    </div>
  );
}

export default App;
