import MUIDataTable from "mui-datatables";
import { Delete, Edit, RemoveRedEye} from '@material-ui/icons';

const TableComp = () => {
	const columns = [
		{
			name: "sno",
			label: "S No."
		},
		{
			name: "docid",
			label: "Lease Document ID"
		},
		{
			name: "date",
			label: "Upload Date"
		},
		{
			name: "status",
			label: "Status"
		},
		{
			name: "latmod",
			label: "Last Modified"
		},
		{
			name: "action",
			label: "Action"
		},
	];

	const data = [
		{
			sno: 1,
			docid:'Name 1',
			date:'01/02/2020',
			status:'Processed',
			latmod: '01/02/2020 2:00 PM',
			action: <>
				<Edit />
				<RemoveRedEye />
				<Delete />
			</>,
		},
		{
			sno: 2,
			docid: 'Name 2',
			date: '01/02/2020',
			status: 'Processed',
			latmod: '01/02/2020 2:00 PM',
			action: <>
				<Edit />
				<RemoveRedEye />
				<Delete />
			</>,
		},
		{
			sno: 3,
			docid: 'Name 3',
			date: '01/02/2020',
			status: 'Processed',
			latmod: '01/02/2020 2:00 PM',
			action: <>
				<Edit />
				<RemoveRedEye />
				<Delete />
			</>,
		}
	];

	const options = {
		filterType: "select",
		download: false,
		print: false,
		filter: false,
		viewColumns: false,
		searchOpen:true,
		selectableRows:false,
		customToolbar:()=><div>
			<select onChange={(val)=>{
				console.log('select', val.target.value);
			}}>
				<option>Status</option>
				<option>1</option>
				<option>2</option>
			</select>
		</div>
	};

	return (
		<MUIDataTable
			title={""}
			searchOpen={true}
			data={data}
			columns={columns}
			options={options}
		/>
	);
}

export default TableComp;